const { NodeHtmlMarkdown } = require('node-html-markdown');
const request = require('request');

class MattermostClient {
  send(receiver, message) {
    request.post({
      uri: receiver.url + '/api/v4/posts',
      body: {
        channel_id: receiver.channel_id,
        message: NodeHtmlMarkdown.translate(message)
      },
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + receiver.token 
      },
      json: true
    });
  }

  sendError(receiver, e) {
    this.send(receiver, `Error: ${e}`);
  }
}

module.exports = MattermostClient;