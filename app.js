const Docker = require('dockerode');
const TelegramClient = require('./telegram');
const MattermostClient = require('./mattermost');
const JSONStream = require('JSONStream');
const templates = require('./templates');
const fs = require('fs');

const { ONLY_WHITELIST } = process.env;

let receiver = [];

async function sendEvent(receiver, event) {
  // console.debug(event);
  const template = templates[`${event.Type}_${event.Action}`];
  if (template) {
    const label = event.Actor && event.Actor.Attributes && event.Actor.Attributes['docker-notifier.monitor'];
    const shouldMonitor = label === undefined ? undefined : label.toLowerCase().trim() !== 'false';
    if (shouldMonitor || !ONLY_WHITELIST && shouldMonitor !== false) {
      const attachment = template(event);
      console.log(attachment, "\n");
      await send(receiver, attachment)
    }
  }
}

async function sendEventStream(receiver, docker) {
  const eventStream = await docker.getEvents();
  eventStream.pipe(JSONStream.parse())
    .on('data', event => sendEvent(receiver, event).catch(handleError))
    .on('error', handleError);
}

async function sendVersion(receiver, docker) {
  const version = await docker.version();
  let text = `Connected to docker ${version.Version}`;
  console.log(text, "\n");
  await send(receiver, text);
}

async function main() {
  const config = getConfig();
  receiver = config.receiver.map(receiver => {
    if (receiver.type == 'telegram') {
      receiver['object'] = new TelegramClient(receiver.bot_token);
    } else if (receiver.type == 'mattermost') {
      receiver['object'] = new MattermostClient(receiver.url);
    }
    return receiver;
  });
  const docker = getDockerInfo(config);
  docker.forEach(async docker => {
    await sendVersion(receiver, docker);
    await sendEventStream(receiver, docker);
  });
}

function getConfig() {
  return JSON.parse(fs.readFileSync(process.env.CONFIG_FILE || '/etc/docker-notifier/config.json'));
}

function getDockerInfo(config) {
  return config.docker.map(docker => new Docker({
    socketPath: docker.socketPath,
    protocol: docker.protocol,
    host: docker.host,
    port: docker.port,
    ca: fs.readFileSync(docker.ca || '/etc/docker-notifier/cert/ca.pem'),
    cert: fs.readFileSync(docker.cert || '/etc/docker-notifier/cert/cert.pem'),
    key: fs.readFileSync(docker.key || '/etc/docer-notifier/cert/key.pem')
  }));
}

async function healthcheck() {
  try {
    const config = getConfig();
    const docker = getDockerInfo(config);
    docker.forEach(async docker => {
      await docker.version();
    });
  } catch (e) {
    console.error(e);
    console.error("Docker is unavailable");
    process.exit(101);
  }

  console.log("OK");
  process.exit(0);
}

async function send(receiver, text) {
  receiver.forEach(async receiver => {
    await receiver['object'].send(receiver, text);
  });
}

function handleError(e) {
  console.error(e);
  receiver.forEach(receiver => {
    receiver['object'].sendError(receiver, e).catch(console.error);
  });
}

if (process.argv.includes("healthcheck")) {
  healthcheck();
} else {
  main().catch(handleError);
}