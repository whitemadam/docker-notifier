const { Telegram } = require('telegraf');

class TelegramClient {
  constructor(bot_token) {
    this.telegram = new Telegram(bot_token);
  }

  send(receiver, message) {
    return this.telegram.sendMessage(
      receiver.chat_id,
      message,
      { 
        parse_mode: 'HTML',
        disable_web_page_preview: true
      }
    );
  }

  sendError(receiver, e) {
    return this.telegram.sendMessage(
        receiver.chat_id,
        `Error: ${e}`,
    );
  }

  check(receiver) {
    return this.telegram.getMe(receiver.chat_id);
  }
}

module.exports = TelegramClient;